﻿using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.Services;
using xpos334.viewmodels;
using Xpos334.Services;

namespace xpos334.Controllers
{
    public class RoleController : Controller
    {
        private RoleService roleService;
        private int IdUser = 1;

        public RoleController(RoleService _roleService)
        {
            roleService = _roleService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<TblRole> data = await roleService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.RoleName.ToLower().Contains(searchString.ToLower())).ToList();
                   
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }

            return View(PaginatedList<TblRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }
        public async Task<IActionResult> Index_MenuAccess(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<TblRole> data = await roleService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.RoleName.ToLower().Contains(searchString.ToLower())).ToList();

            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }

            return View(PaginatedList<TblRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }
        public IActionResult Create()
        {
            TblRole data = new TblRole();
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TblRole dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await roleService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string roleName, int id)
        {
            bool isExist = await roleService.CheckNameRoleByName(roleName, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }
        public async Task<IActionResult> Edit_MenuAccess(int id)
        {
            VMTblRole data = await roleService.GetDataById_MenuAccess(id);
            ViewBag.role_menu = data.role_menu;
            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(TblRole dataParam)
        {
            dataParam.UpdatedBy = IdUser;
            VMResponse respon = await roleService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        [HttpPost]
        public async Task<IActionResult> Edit_MenuAccess(VMTblRole dataParam)
        {
            dataParam.UpdatedBy = IdUser;
            VMResponse respon = await roleService.Edit_MenuAccess(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int createdBy = IdUser;
            VMResponse respon = await roleService.Delete(id, createdBy);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }
       
    }
}
