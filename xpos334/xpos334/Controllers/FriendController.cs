﻿using Microsoft.AspNetCore.Mvc;
using xpos334.Models;

namespace xpos334.Controllers
{
    public class FriendController : Controller
    {
       private static List<Friend> friends = new List<Friend>()
            {
                new Friend(){Id = 1, Name = "Irvan", Address = "Kemayoran"},
                new Friend(){Id = 2, Name = "Tunggul", Address = "Cilandak"},
                new Friend(){Id = 3, Name = "Sabrina", Address = "Radio Dalam"}
            };

        public IActionResult Index()
        {
            /*List<Friend> friends = new List<Friend>()
            {
                new Friend(){Id = 1, Name = "Irvan", Address = "Kemayoran"},
                new Friend(){Id = 2, Name = "Tunggul", Address = "Cilandak"},
                new Friend(){Id = 3, Name = "Sabrina", Address = "Radio Dalam"}
            };*/
            ViewBag.listFriend = friends;
            return View(friends);
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            friends.Add(friend);

            return RedirectToAction("Index");
        }
        public IActionResult Edit(int id) 
        {
            //Friend friend = friends[id];
            Friend friend = friends.Find(a => a.Id == id)!;
            return View(friend);
        }
        [HttpPost]
        public IActionResult Edit(Friend data)
        {
            Friend friend = friends.Find(a => a.Id == data.Id);
            int index = friends.IndexOf(friend);
            if (index >-1)
            {
                friends[index].Name = data.Name;
                friends[index].Address = data.Address;
            }
            return RedirectToAction("Index");
        }
        public IActionResult Detail(int id)
        {
            //Friend friend = friends[id];
            Friend friend = friends.Find(a => a.Id == id)!;
            return View(friend);
        }

        public IActionResult Delete(int id)
        {
            //Friend friend = friends[id];
            Friend friend = friends.Find(a => a.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        
        public IActionResult Delete(Friend data) 
        {
            Friend friend = friends.Find(a => a.Id == data.Id);
            friends.Remove(friend);
            return RedirectToAction("Index");
        }
    }
}
