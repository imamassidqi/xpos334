﻿using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;
using Xpos334.Services;

namespace Xpos334.Controllers

{
    public class CategoryTryController : Controller
    {

        private readonly XPOS_334Context db;
        private readonly CategoryTryService categoryServices;

        public CategoryTryController(XPOS_334Context _db)
        {
            db = _db;
            this.categoryServices = new CategoryTryService(db);
        }

        public IActionResult Index()
        {
            List<VMTblCategory> dataView = categoryServices.GetAllData();
            return View(dataView);
        }

        public IActionResult Create() 
        {
            VMTblCategory dataView = new VMTblCategory();
            return PartialView(dataView);
        }

        [HttpPost]

        public IActionResult Create(VMTblCategory dataView)
        {
            VMResponse respon = new VMResponse();
            if (ModelState.IsValid)
            {
                respon = categoryServices.Create(dataView);
                    if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Edit(long id) 
        {
            VMTblCategory dataView = categoryServices.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]
        public IActionResult Edit (VMTblCategory dataView)
        {
            VMResponse respon = new VMResponse();
            if (ModelState.IsValid)
            {
                respon = categoryServices.Edit(dataView);
                if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            respon.Entity = dataView;
            return View(respon.Entity);
        }
        public IActionResult Detail(long id)
        {
            VMTblCategory dataView = categoryServices.GetById(id);
            //ViewBag.tbl_category = dataView;
            //ViewBag.datalist = dataView;
            return PartialView(dataView);
        }

        public IActionResult Delete(long id) 
        {
            VMTblCategory dataView = categoryServices.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]

        public IActionResult Delete(VMTblCategory dataView)
        {
            VMResponse respon = categoryServices.Delete(dataView);
            if (respon.Success)
            {
                return RedirectToAction("Index");
            }
            return View(respon.Entity);
        }

    }
}