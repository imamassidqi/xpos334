﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using xpos334.datamodels;
using xpos334.viewmodels;
using Xpos334.Services;

namespace Xpos334.Controllers
{
    public class VariantTryController : Controller
    {
        private readonly XPOS_334Context db;
        private readonly CategoryTryService categoryService;
        private readonly VariantTryService variantTryService;

        private static VMPage page = new VMPage();

        public VariantTryController(XPOS_334Context _db)
        {
            db = _db;
            categoryService = new CategoryTryService(db);
            variantTryService = new VariantTryService(db);
        }

        public IActionResult Index(VMPage pg)
        {
            ViewBag.idSort = string.IsNullOrEmpty(pg.sortOrder) ? "id_desc" : "";
            ViewBag.nameSort = pg.sortOrder == "name" ? "name_desc" : "name";
            ViewBag.CurrentSort = pg.sortOrder;
            ViewBag.currentShowData = pg.showData;

            if (pg.searchString != null)
            {
                pg.pageNumber = 1;
            }
            else
            {
                pg.searchString = pg.currentFilter;
            }

            ViewBag.CurrentFilter = pg.currentFilter;

            List<VMTblVariant> dataView = variantTryService.GetAllData();

            if (!string.IsNullOrEmpty(pg.searchString))
            {
                dataView = dataView.Where(a => a.NameVariant.ToLower().Contains(pg.searchString.ToLower())
                ).ToList();
            }

            switch (pg.sortOrder)
            {
                case "name_desc":
                    dataView = dataView.OrderByDescending(a => a.NameVariant).ToList();
                    break;
                case "name":
                    dataView = dataView.OrderBy(a => a.NameVariant).ToList();
                    break;
                case "id_desc":
                    dataView = dataView.OrderByDescending(a => a.Id).ToList();
                    break;
                default:
                    dataView = dataView.OrderBy(a => a.Id).ToList();
                    break;

            }

            int pageSize = pg.showData ?? 3;

            page = pg;


            return View(PaginatedList<VMTblVariant>.CreateAsync(dataView, pg.pageNumber ?? 1, pageSize));
        }

        public IActionResult Create()
        {
            VMTblVariant dataView = new VMTblVariant();
            ViewBag.DropdownCategory = categoryService.GetAllData();
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Create(VMTblVariant dataView)
        {
            VMResponse respon = new VMResponse();

            respon = variantTryService.Create(dataView);

            if (respon.Success)
            {
                return RedirectToAction("Index", page);
            }
            ViewBag.DropdownCategory = categoryService.GetAllData();
            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            ViewBag.DropdownCategory = categoryService.GetAllData();
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Edit(VMTblVariant dataView)
        {
            VMResponse respon = new VMResponse();

            respon = variantTryService.Edit(dataView);

            if (respon.Success)
            {
                return RedirectToAction("Index", page);
            }
            ViewBag.DropdownCategory = categoryService.GetAllData();
            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Detail(int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            return View(dataView);
        }

        public IActionResult Delete(int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            return View(dataView);
        }

        [HttpPost]

        public IActionResult Delete(VMTblVariant dataView)
        {
            VMResponse respon = variantTryService.Delete(dataView);
            if (respon.Success)
            {
                return RedirectToAction("Index", page);
            }
            return View(respon.Entity);
        }
    }
}
