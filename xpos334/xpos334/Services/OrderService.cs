﻿using Newtonsoft.Json;
using System.Drawing;
using System.Text;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class OrderService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        private string json;

        public OrderService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }
        public async Task<VMResponse> SubmitOrder(VMOrderHeader dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiOrder/SubmitOrder", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<List<VMOrderHeader>> GetDataOrderHeaderDetail(int IdCustomer)
        {
            List<VMOrderHeader> data = new List<VMOrderHeader>();

            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiOrder/GetDataOrderHeaderDetail/{IdCustomer}");
            data = JsonConvert.DeserializeObject<List<VMOrderHeader>>(apiRespon);
            return data;
        }

        public async Task<int> CountTransaction (int IdCustomer)
        {
          
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/CountTransaction/{IdCustomer}");
            int data = JsonConvert.DeserializeObject<int>(apiResponse);

            return data;
        }
    }
   

}
