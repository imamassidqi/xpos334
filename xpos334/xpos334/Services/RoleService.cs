﻿using Newtonsoft.Json;
using System.Text;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class RoleService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public RoleService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<TblRole>> GetAllData()
        {
            List<TblRole> data = new List<TblRole>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiRole/GetAllData");
            data = JsonConvert.DeserializeObject<List<TblRole>>(apiResponse);

            return data;

        }

        public async Task<VMResponse> Create(TblRole dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiRole/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
        public async Task<bool> CheckNameRoleByName(string roleName, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiRole/CheckNameRoleByName/{roleName}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }

        public async Task<TblRole> GetDataById(int id)
        {
            TblRole data = new TblRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/GetDataById/{id}"); //get url API
            data = JsonConvert.DeserializeObject<TblRole>(apiResponse); //deserialis mengubah json menjadi objek, serialis sebaliknya
            return data;
        }

        public async Task<VMTblRole> GetDataById_MenuAccess(int id)
        {
            VMTblRole data = new VMTblRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/GetDataById_MenuAccess/{id}"); //get url API
            data = JsonConvert.DeserializeObject<VMTblRole>(apiResponse); //deserialis mengubah json menjadi objek, serialis sebaliknya
            return data;
        }

        public async Task<VMResponse> Edit(TblRole dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiRole/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Edit_MenuAccess(VMTblRole dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiRole/Edit_MenuAccess", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiRole/Delete/{id}/{createBy}");

            if (request.IsSuccessStatusCode)
            {

                // ini adalah proses baca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke Objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
