﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Drawing.Printing;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCategoryController(XPOS_334Context _db)
        {
                db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblCategory> GetAllData()
        {
            List<TblCategory> data = db.TblCategories.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblCategory DataById(int id)
        {
            TblCategory result = db.TblCategories.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("CheckCategoryByName/{name}/{id}")]
        public bool CheckName(string name, int id) 
        {
            TblCategory data = new TblCategory();
            if (id == 0)//ini untuk saat create
            {
                data = db.TblCategories.Where(a => a.NameCategory == name && a.IsDelete == false).FirstOrDefault();
            }
            else // untuk edit 
            {
                data = db.TblCategories.Where(a => a.NameCategory == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }
            if(data != null) 
            {
                return true;
            }
            return false;
        }
        [HttpPost("Save")]

        public VMResponse Save(TblCategory data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;
            data.Description = data.Description ?? "";

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex) 
            {
                respon.Success = false;
                respon.Message = "Failed saved" + ex.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCategory data)
        {
            TblCategory dt = db.TblCategories.Where(a => a.Id == data.Id).FirstOrDefault();
            if (dt != null) 
            {
                dt.NameCategory = data.NameCategory;
                dt.Description = data.Description;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + ex.Message;
                }
                return respon;
            }
            else 
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
           
        }

        [HttpDelete("Delete/{id}/{createBy}")]

        public VMResponse Delete(int id, int createBy)
        {
            TblCategory dt = db.TblCategories.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {

                dt.IsDelete = true;
                dt.UpdateBy = createBy;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
                return respon;
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;

        }

    }
}
