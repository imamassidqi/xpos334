﻿using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Services
{
    public class RolesService
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();

        public RolesService(XPOS_334Context _db)
        {
            this.db = _db;
        }

        public async Task<List<VMMenuAccess>> GetMenuAccessParentChildByRoleID(int IdRole, int MenuParent, bool OnlySelected = false)
        {
            List<VMMenuAccess> result = new List<VMMenuAccess>();
            List<TblMenu> data = db.TblMenus.Where(a => a.MenuParent == MenuParent && a.IsDelete == false).ToList();
            foreach (TblMenu item in data)
            {
                VMMenuAccess list = new VMMenuAccess();
                list.IdMenu = item.Id;
                list.MenuName = item.MenuName;
                list.IsParent = item.IsParent;
                list.MenuParent = item.MenuParent;
                list.is_selected = db.TblMenuAccesses.Where(a => a.RoleId == IdRole && a.MenuId == item.Id && a.IsDelete == false).Any();
                list.ListChild = await GetMenuAccessParentChildByRoleID(IdRole, item.Id, OnlySelected);
                if (OnlySelected)
                {
                    if (list.is_selected)
                        result.Add(list);
                }
                else
                {
                    result.Add(list);
                }
            }
            return result;
        }
    }
}
